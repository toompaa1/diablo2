﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Threading.Tasks;
using Diablo2;


namespace Diablo2Test
{
    public class HeroTest
    {
        [Fact]
        public void Create_Hero_See_If_Its_LevelOne()
        {
            //Arrange
            Mage mageOne = new Mage("Ella");
            int levelExpected = 1;

            //Act

            //Assert
            Assert.Equal(levelExpected, mageOne.Level);
        }

        [Fact]
        public void Hero_Gain_One_Level()
        {
            //Arrange
            Mage mageOne = new Mage("Ella");
            int levelExpected = 2;

            //Act
            mageOne.LevelUp(1);

            //Assert
            Assert.Equal(levelExpected, mageOne.Level);

        }

        [Fact]
        public void Check_Mage_Level_One_Attributes()
        {
            //Arrange
            Mage mageOne = new Mage("Ella");
            mageOne.BaseAttributes();
            int vitality = 5;
            int dexterity = 1;
            int strength = 1;
            int intelligence = 8;
            //Act
            int actualVitality = mageOne.primaryAttributes.Vitality;
            int actualDexterity = mageOne.primaryAttributes.Dexterity;
            int actualIntelligence = mageOne.primaryAttributes.Intelligence;
            int actualStrength = mageOne.primaryAttributes.Strength;

            //Assert
            Assert.Equal(vitality, actualVitality);
            Assert.Equal(dexterity, actualDexterity);
            Assert.Equal(strength, actualStrength);
            Assert.Equal(intelligence, actualIntelligence);
        }

        [Fact]
        public void Check_Rogue_Level_One_Attributes()
        {
            //Arrange
            Rogue rogue = new Rogue("Ella");
            rogue.BaseAttributes();
            int vitality = 8;
            int dexterity = 6;
            int strength = 2;
            int intelligence = 1;
            //Act
            int actualVitality = rogue.primaryAttributes.Vitality;
            int actualDexterity = rogue.primaryAttributes.Dexterity;
            int actualIntelligence = rogue.primaryAttributes.Intelligence;
            int actualStrength = rogue.primaryAttributes.Strength;

            //Assert
            Assert.Equal(vitality, actualVitality);
            Assert.Equal(dexterity, actualDexterity);
            Assert.Equal(strength, actualStrength);
            Assert.Equal(intelligence, actualIntelligence);
        }

        [Fact]
        public void Check_Ranger_Level_One_Attributes()
        {
            //Arrange
            Ranger ranger = new Ranger("Ella");
            ranger.BaseAttributes();
            int vitality = 8;
            int dexterity = 7;
            int strength = 1;
            int intelligence = 1;
            //Act
            int actualVitality = ranger.primaryAttributes.Vitality;
            int actualDexterity = ranger.primaryAttributes.Dexterity;
            int actualIntelligence = ranger.primaryAttributes.Intelligence;
            int actualStrength = ranger.primaryAttributes.Strength;

            //Assert
            Assert.Equal(vitality, actualVitality);
            Assert.Equal(dexterity, actualDexterity);
            Assert.Equal(strength, actualStrength);
            Assert.Equal(intelligence, actualIntelligence);
        }

        [Fact]
        public void Check_Warrior_Level_One_Attributes()
        {
            //Arrange
            Warrior warrior = new Warrior("Ella");
            warrior.BaseAttributes();
            int vitality = 10;
            int dexterity = 2;
            int strength = 5;
            int intelligence = 1;
            //Act
            int actualVitality = warrior.primaryAttributes.Vitality;
            int actualDexterity = warrior.primaryAttributes.Dexterity;
            int actualIntelligence = warrior.primaryAttributes.Intelligence;
            int actualStrength = warrior.primaryAttributes.Strength;

            //Assert
            Assert.Equal(vitality, actualVitality);
            Assert.Equal(dexterity, actualDexterity);
            Assert.Equal(strength, actualStrength);
            Assert.Equal(intelligence, actualIntelligence);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Check_If_Class_Can_Level_With_Zero_Or_Negative_Number(int InlineData)
        {
            //Arrange
            Mage mageOne = new Mage("Mage");
            //Act


            //Assert
            Assert.Throws<ArgumentException>(() => mageOne.LevelUp(InlineData));
        }

        [Fact]
        public void Check_If_Mage_Gain_Attributes_When_Levling()
        {
            //Arrange
            Mage mage = new Mage("Mage");
            mage.LevelUp(1);

            //Act
            int expectedVitality = 8;
            int expectedDexterity = 2;
            int expectedIntelligence = 13;
            int expectedStrength = 2;
            //Assert
            Assert.Equal(expectedStrength, mage.primaryAttributes.Strength);
            Assert.Equal(expectedVitality, mage.primaryAttributes.Vitality);
            Assert.Equal(expectedIntelligence, mage.primaryAttributes.Intelligence);
            Assert.Equal(expectedDexterity, mage.primaryAttributes.Dexterity);
        }

        [Fact]
        public void Check_If_Ranger_Gain_Attributes_When_Levling()
        {
            //Arrange
            Ranger ranger = new Ranger("Ranger");
            ranger.LevelUp(1);

            //Act
            int expectedVitality = 10;
            int expectedDexterity = 12;
            int expectedIntelligence = 2;
            int expectedStrength = 2;
            //Assert
            Assert.Equal(expectedStrength, ranger.primaryAttributes.Strength);
            Assert.Equal(expectedVitality, ranger.primaryAttributes.Vitality);
            Assert.Equal(expectedIntelligence, ranger.primaryAttributes.Intelligence);
            Assert.Equal(expectedDexterity, ranger.primaryAttributes.Dexterity);
        }

        [Fact]
        public void Check_If_Rogue_Gain_Attributes_When_Levling()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            Rogue rogue = new Rogue("Rogue");
            warrior.LevelUp(1);
            rogue.LevelUp(1);

            //Act
            int expectedVitality = 11;
            int expectedDexterity = 10;
            int expectedIntelligence = 2;
            int expectedStrength = 3;
            //Assert
            Assert.Equal(expectedStrength, rogue.primaryAttributes.Strength);
            Assert.Equal(expectedVitality, rogue.primaryAttributes.Vitality);
            Assert.Equal(expectedIntelligence, rogue.primaryAttributes.Intelligence);
            Assert.Equal(expectedDexterity, rogue.primaryAttributes.Dexterity);
        }

        [Fact]
        public void Check_If_Warrior_Gain_Attributes_When_Levling()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            warrior.LevelUp(1);

            //Act
            int expectedVitality = 15;
            int expectedDexterity = 4;
            int expectedIntelligence = 2;
            int expectedStrength = 8;
            //Assert
            Assert.Equal(expectedStrength, warrior.primaryAttributes.Strength);
            Assert.Equal(expectedVitality, warrior.primaryAttributes.Vitality);
            Assert.Equal(expectedIntelligence, warrior.primaryAttributes.Intelligence);
            Assert.Equal(expectedDexterity, warrior.primaryAttributes.Dexterity);
        }

        [Fact]
        public void Check_SecondaryAttributed_On_Warrior_After_One_level()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            warrior.LevelUp(1);
            //Act
            int expectedHealth = 150;
            int expectedArmorRating = 12;
            int expectedElementalResistance = 2;
            //Assert
            Assert.Equal(expectedHealth, warrior.secondaryAttributes.Health);
            Assert.Equal(expectedArmorRating, warrior.secondaryAttributes.ArmorRating);
            Assert.Equal(expectedElementalResistance, warrior.secondaryAttributes.ElementalResistance);
        }
    }
}
