﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diablo2;
using Xunit;

namespace Diablo2Test
{
    public class EquipmentTest
    {
        [Fact]
        public void Check_If_InvalidWeaponException_Is_Thrown()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act           

            //Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon("Axe", 2, 1, 1, WeaponType.Axe, ItemSlot.Weapon));
        }

        [Fact]
        public void Check_If_InvalidArmorException_Is_Thrown()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act           

            //Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor("Platebody", 2, new PrimaryAttributes() { Dexterity = 5, Intelligence = 10, Strength = 5, Vitality = 7 }, ArmorType.Plate, ItemSlot.Body));
        }

        [Fact]
        public void Check_If_Warrior_Can_Use_Wrong_WeaponType_Exception_Should_Be_Thrown()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act

            //Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon("Axe", 2, 1, 1, WeaponType.Wand, ItemSlot.Weapon));
        }

        [Fact]
        public void Check_If_Warrior_Can_Use_Wrong_ArmourType_Exception_Should_Be_Thrown()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act

            //Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor("Platebody", 2, new PrimaryAttributes() { Dexterity = 5, Intelligence = 10, Strength = 5, Vitality = 7 }, ArmorType.Cloth, ItemSlot.Body));
        }

        [Fact]
        public void Check_If_EquipWeapon_Returns_Message()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act
            string expectedMessage = "New weapon equipped!";
            //Assert
            Assert.Equal(expectedMessage, warrior.EquipWeapon("Axe", 1, 1, 1, WeaponType.Axe, ItemSlot.Weapon));
        }

        [Fact]
        public void Check_If_EquipArmour_Returns_Message()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            warrior.EquipWeapon("Axe", 1, 1, 1, WeaponType.Axe, ItemSlot.Weapon);
            //Act
            string expectedMessage = "New armour equipped!";
            //Assert
            Assert.Equal(expectedMessage, warrior.EquipArmor("Platebody", 1, new PrimaryAttributes() { Dexterity = 5, Intelligence = 5, Strength = 5, Vitality = 5 }, ArmorType.Plate, ItemSlot.Body));
        }

        [Fact]
        public void Check_Calculate_Hero_Dps_At_Lvl_One_With_No_Weapon()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            //Act
            int expectedDps = 1 * (1 + (5 / 100));
            //Assert
            Assert.Equal(expectedDps, warrior.HeroDps);
        }

        [Fact]
        public void Check_Calculate_Hero_Dps_At_lvl_One_With_Weapon()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            warrior.EquipWeapon("Axe", 1, 7, 1.1, WeaponType.Axe, ItemSlot.Weapon);
            //Act
            double expectedDpsWithWeapon = (7 * 1.1 * (1 + (5 / 100)));
            //Assert
            Assert.Equal(expectedDpsWithWeapon, warrior.HeroDps);
        }

        [Fact]
        public void Check_Calculate_Hero_Dps_At_Lvl_One_With_Armour_And_Weapon_Used()
        {
            //Arrange
            Warrior warrior = new Warrior("Warrior");
            warrior.EquipWeapon("Axe", 1, 7, 1.1, WeaponType.Axe, ItemSlot.Weapon);
            warrior.EquipArmor("PlateBody", 1, new PrimaryAttributes() { Strength = 1, Vitality = 2 }, ArmorType.Plate, ItemSlot.Body);
            //Act
            double expectedDpsWithWeaponAndArmour = (7 * 1.1) * (1 + ((5.0 + 1) / 100.0));

            Assert.Equal(expectedDpsWithWeaponAndArmour, warrior.HeroDps);
        }

    }
}
