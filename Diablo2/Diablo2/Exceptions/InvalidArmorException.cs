﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    /// <summary>
    /// Custom exception class for ArmorException.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {
            Console.WriteLine("You cant equip this armour");
        }
    }
}
