﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Hero constructor that takes a name as parameter so that we can give the mage a name.
        /// Setting hero to lvl 1 and the dps to 1 if not using a weapon. 
        /// Runs the Baseattributes and SecondAttributes just soo that we can get the stats of the hero.
        /// </summary>
        public Ranger(string _name)
        {
            Name = _name;
            Level = 1;
            HeroDps = 1;
            BaseAttributes();
            SecondaryAttributes();
        }

        /// <summary>
        /// Method that runs from the constructor when creating a new class of the hero.
        /// Assigning the base lvl 1 attributes to the hero.
        /// </summary>
        public override void BaseAttributes()
        {
            primaryAttributes.Vitality = 8;
            primaryAttributes.Strength = 1;
            primaryAttributes.Dexterity = 7;
            primaryAttributes.Intelligence = 1;
        }

        /// <summary>
        /// Method that will increase the lvl of the hero and calculate the primaryattributes after lvling.
        /// After every lvlup im calling on 2 functions that will calculate secondaryattributes and the total of primaryattributes.
        /// After that i call the PrintStats just to see if the hero gained anything.
        /// </summary>
        public override void LevelUp(int level)
        {
            if (level > 0)
            {
                primaryAttributes.Vitality += (2 * level);
                primaryAttributes.Strength += (1 * level);
                primaryAttributes.Dexterity += (5 * level);
                primaryAttributes.Intelligence += (1 * level);
                Level = level;
                Console.WriteLine("----------LEVEL UP----------");
                SecondaryAttributes();
                PrintStats();
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Equip weapon method that every hero has. When using it "mage.EquipWeapon()" We need to pass in name, level, baseDmg, baseAttSpeed, weapontype and itemSlot.
        /// When calling on the EquipWeapon it creates a weapon based on the parameters u passed in. Then it checks if the hero has required level to use the weapon.
        /// Also checks if the weapontype equals the right weapon type for the hero.
        /// If its not equal invalidWeaponException should be thrown. 
        /// If requirements are true add the the item to the equipmentList, and then calculate the dps for the hero.
        /// </summary>
        /// <param name="name">Sets the name of the weapon</param>
        /// <param name="level">Sets the requierd level to the weapon</param>
        /// <param name="baseDmg">Sets the basedmg to the weapon</param>
        /// <param name="baseAttSpeed">Sets the attspeed to the weapon</param>
        /// <param name="weapon">Sets the weapontype to the weapon</param>
        /// <param name="slot">Sets the itemslot to the weapon</param>
        /// <returns>Returns a message</returns>
        public override string EquipWeapon(string name, int level, int baseDmg, double baseAttSpeed, WeaponType weapon, ItemSlot slot)
        {
            string message = "New weapon equipped!";
            Weapon rangeWeapon = new Weapon(name, level, baseDmg, baseAttSpeed, weapon, slot);
            if (Level >= level && (weapon.Equals(WeaponType.Bows)) && (slot.Equals(ItemSlot.Weapon)))
            {
                rangeWeapon.PrintWeapon();
                HeroDps = rangeWeapon.CalculateDps() * (1 + primaryAttributes.Dexterity / 100);
                equipmentList.Add(ItemSlot.Weapon, rangeWeapon);
                return message;
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        /// <summary>
        /// Equip armour method that every hero has. When using it "mage.EquipArmor()" We need to pass in name, level, attributes, armortype, itemSlot.
        /// When calling on the EquipWeapon it creats a armor based on the parameters u passed in. Then it checks if the hero has requierd level to use the weapon.
        /// Also checks if the armortype and itemslot equals the right for th hero.
        /// If its not equa invalidArmorException should be thrown.
        /// If requierments are true add the item to the equipmentList and add the attributes from the armor to heros primary attributes. And then Caluculate the herodps again. 
        /// </summary>
        /// <param name="name">Sets the name of the armour</param>
        /// <param name="level">Sets required level to the armour</param>
        /// <param name="attributes">Sets the attributes to the armour</param>
        /// <param name="armor">Sets the amourtype to the armour</param>
        /// <param name="slot">Sets itemslot to the armour</param>
        /// <returns>Returns a message</returns>
        public override string EquipArmor(string name, int level, PrimaryAttributes attributes, ArmorType armor, ItemSlot slot)
        {
            string message = "New armour equipped!";
            Armor a1 = new Armor(name, level, attributes, armor, slot);
            if (Level >= level && (armor.Equals(ArmorType.Leather)) || (armor.Equals(ArmorType.Mail)) && a1.ItemSlot.Equals(ItemSlot.Body) || a1.ItemSlot.Equals(ItemSlot.Head) || a1.ItemSlot.Equals(ItemSlot.Legs))
            {
                if (equipmentList.ContainsKey(slot))
                {
                    equipmentList[slot] = a1;
                }
                else
                {
                    equipmentList.Add(slot, a1);
                    Console.WriteLine($"Equipt: {name}");
                    Console.WriteLine($"Slot: {slot}");
                }
                primaryAttributes.Vitality += attributes.Vitality;
                primaryAttributes.Dexterity += attributes.Dexterity;
                primaryAttributes.Strength += attributes.Strength;
                primaryAttributes.Intelligence += attributes.Intelligence;
                var weaponDps = ((Weapon)equipmentList[ItemSlot.Weapon]).CalculateDps();
                HeroDps = weaponDps * ((1 + ((double)this.primaryAttributes.Dexterity / 100)));
                return message;
            }
            else
            {
                throw new InvalidArmorException();
            }
        }
    }
}
