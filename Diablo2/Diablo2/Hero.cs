﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    public abstract class Hero
    {
        /// <summary>
        /// Declaring properties for Hero class.
        /// </summary>
        public string Name { get; set; }
        public int Level { get; set; }
        public int TotalPrimaryAttributes { get; set; }
        public double HeroDps { get; set; }

        public SecondaryAttributes secondaryAttributes = new SecondaryAttributes();

        public PrimaryAttributes primaryAttributes = new PrimaryAttributes();

        public Dictionary<ItemSlot, Equipment> equipmentList = new Dictionary<ItemSlot, Equipment>();

        /// <summary>
        /// Method that will print out the stats of the hero.
        /// </summary>
        public void PrintStats()
        {
            Console.WriteLine("----------CHAMPION----------");
            Console.WriteLine($"Champion: {Name}");
            Console.WriteLine($"Level: {Level}");
            Console.WriteLine($"Vitality: {primaryAttributes.Vitality}");
            Console.WriteLine($"Strength: {primaryAttributes.Strength}");
            Console.WriteLine($"Dexterity: {primaryAttributes.Dexterity}");
            Console.WriteLine($"Intelligence: {primaryAttributes.Intelligence}");
            Console.WriteLine($"Health: {secondaryAttributes.Health}");
            Console.WriteLine($"Armor rating: {secondaryAttributes.ArmorRating}");
            Console.WriteLine($"Elemental resistance {secondaryAttributes.ElementalResistance}");
            Console.WriteLine($"DPS: {HeroDps}");
        }

        /// <summary>
        /// Method that will calculate secondaryAttributes based on primaryAttributes.
        /// </summary>
        public void SecondaryAttributes()
        {
            secondaryAttributes.Health = primaryAttributes.Vitality * 10;
            secondaryAttributes.ArmorRating = primaryAttributes.Strength + primaryAttributes.Dexterity;
            secondaryAttributes.ElementalResistance = primaryAttributes.Intelligence;
        }

        /// <summary>
        /// Method for levling up heros. Takes in a paramater for how many lvls a hero should lvl up.
        /// </summary>
        public abstract void LevelUp(int level);

        /// <summary>
        /// Methos for equiping weapon for all the heros, takes in paramaters just for creating the weapon.
        /// /// But changes based on class
        /// </summary>
        public abstract string EquipWeapon(string name, int level, int baseDmg, double baseAttSpeed, WeaponType weapon, ItemSlot slot);

        /// <summary>
        /// Methos for equiping armour for all the heros, takes in paramaters just for creating the armour.
        /// But changes based on class
        /// </summary>
        public abstract string EquipArmor(string name, int level, PrimaryAttributes primaryAttributes, ArmorType armor, ItemSlot slot);

        /// <summary>
        /// Method for all the classes but changes bases on class.
        /// </summary>
        public abstract void BaseAttributes();
    }
}
