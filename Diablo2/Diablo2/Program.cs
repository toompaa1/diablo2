﻿using System;

namespace Diablo2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Try  => catch just to see if an exception is thrown.
            try
            {
                Mage mage = new Mage("Ella");
                mage.LevelUp(2);
                mage.EquipArmor("Steel platebody", 10, new PrimaryAttributes() { Dexterity = 5, Intelligence = 10, Strength = 5, Vitality = 7 }, ArmorType.Cloth, ItemSlot.Body);
                mage.PrintStats();
            }
            catch (InvalidWeaponException)
            {

            }
        }
    }
}
