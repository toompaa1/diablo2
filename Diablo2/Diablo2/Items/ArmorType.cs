﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    /// <summary>
    /// Enum class for ArmourType. Can be reached by "ArmorType.Cloth"
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
