﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    /// <summary>
    /// Enum class for slot. Can reach it by "ItemSlot.Weapon".
    /// </summary>
    public enum ItemSlot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
