﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    /// <summary>
    /// Enum class for WeaponType. Can be reached by "WeaponType.Axe"
    /// </summary>
    public enum WeaponType
    {
        Axe,
        Bows,
        Dagger,
        Staff,
        Sword,
        Wand,
        Hammer
    }
}
