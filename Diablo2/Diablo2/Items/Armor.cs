﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    class Armor : Equipment
    {
        public PrimaryAttributes PrimaryAttributes;

        /// <summary>
        /// When creating an armour we need to pass in name, level, primary attributes, armourtype and itemslot. 
        /// Then it will take thoose values and set the properties with the value.
        /// </summary>
        /// <param name="_name">Sets the passed Name to _name</param>
        /// <param name="_level">Sets Sets Requierd to _level</param>
        /// <param name="primaryAttributes">Sets the primary attributes to primaryAttributes</param>
        /// <param name="armor">Sets ArmorType to armor</param>
        /// <param name="slot">Sets ItemSlot to slot</param>
        public Armor(string _name, int _level, PrimaryAttributes primaryAttributes, ArmorType armor, ItemSlot slot)
        {
            Name = _name;
            RequiredLevel = _level;
            ArmorType = armor;
            PrimaryAttributes = primaryAttributes;
            ItemSlot = slot;
        }

        /// <summary>
        /// Method that will print out the stats of the armour.
        /// </summary>
        public void PrintArmor()
        {
            Console.WriteLine("----------ARMOR EQUIPED----------");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Required level: {RequiredLevel}");
            Console.WriteLine($"Weapon type: {ArmorType}");
            Console.WriteLine($"Vitality: {PrimaryAttributes.Vitality}");
            Console.WriteLine($"Strength: {PrimaryAttributes.Strength}");
            Console.WriteLine($"Dexterity: {PrimaryAttributes.Dexterity}");
            Console.WriteLine($"Intelligence: {PrimaryAttributes.Intelligence}");
            Console.WriteLine($"SlotType: {ItemSlot}");
        }
    }
}
