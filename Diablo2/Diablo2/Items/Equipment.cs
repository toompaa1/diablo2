﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    /// <summary>
    /// Get and set properties for Items "Equipment".
    /// </summary>
    public class Equipment
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }

        public ArmorType ArmorType;

        public WeaponType WeaponType;
    }
}
