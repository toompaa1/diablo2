﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diablo2
{
    class Weapon : Equipment
    {
        /// <summary>
        /// Get and set properties for Weapon class.
        /// </summary>
        public int BaseDmg { get; set; }
        public double BaseAttSpeed { get; set; }
        public double Dps { get; set; }

        /// <summary>
        /// When creating a new weapon we need to pass in some values as parameters.
        /// Such as name, level, basedmg, baseattspeed, weapontype and itemslot.
        /// Runs the calculateDps(), just so we can get the dps as fast we created the weapon.
        /// </summary>
        /// <param name="_name">Sets the name of the weapon</param>
        /// <param name="_level">Sets the level requierd to wield the weapon</param>
        /// <param name="baseDmg">Sets the basedmg </param>
        /// <param name="baseAttSpeed">Sets the baseattspeed</param>
        /// <param name="weapon">Sets the weapontype</param>
        /// <param name="slot">Sets the itemslot</param>
        public Weapon(string _name, int _level, int baseDmg, double baseAttSpeed, WeaponType weapon, ItemSlot slot)
        {
            Name = _name;
            RequiredLevel = _level;
            WeaponType = weapon;
            BaseDmg = baseDmg;
            BaseAttSpeed = baseAttSpeed;
            ItemSlot = slot;
            CalculateDps();
        }

        /// <summary>
        /// Method that will print out the stats of the weapon created.
        /// </summary>
        public void PrintWeapon()
        {
            Console.WriteLine("----------WEAPON EQUIPED----------");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Required level: {RequiredLevel}");
            Console.WriteLine($"Weapon type: {WeaponType}");
            Console.WriteLine($"Damage: {BaseDmg}");
            Console.WriteLine($"Attack speed: {BaseAttSpeed}");
            Console.WriteLine($"SlotType: {ItemSlot}");
        }

        /// <summary>
        /// Method that will calculate the Weapon dps based on BaseDmg and BaseAttSpeed.
        /// </summary>
        /// <returns>Returns the value of BaseDmg * BaseAttSpeed</returns>
        public double CalculateDps()
        {
            Dps = BaseDmg * BaseAttSpeed;
            return Dps;
        }
    }
}
